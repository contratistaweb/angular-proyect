import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-whishlist';
  time = new Observable(observer => {
    setInterval(() => observer.next(new Date().toString()), 1000);
    return null;
  });

  constructor(private translate: TranslateService) {
    console.log('***************** get translation');
    this.translate.getTranslation('en').subscribe(x => console.log('x: ' + JSON.stringify(x)));
    this.translate.setDefaultLang('es');
  }

  destinoAgregado(d) {
    // alert(d.nombre);
  }
}
