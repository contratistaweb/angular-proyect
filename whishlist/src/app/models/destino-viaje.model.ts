import { v4 as uuid } from 'uuid';

export class DestinoViaje {

  servicios: string[];
  id = uuid();
  public votes;
  public selected: boolean;

  constructor(public nombre: string, public imagenUrl: string, ) {
    this.servicios = ['pileta', 'desayuno'];
    this.votes = 0;
    this.selected = true;
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  isSelected() {
    return this.selected;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}
